#include <iostream>

#include "futures.hpp"

#include <exe/futures/terminate/get.hpp>

using namespace exe;
using futures::Future;

// What color is your function?
// https://journal.stuffwithstuff.com/2015/02/01/what-color-is-your-function/

// Red (asynchronous)
Future<int> Bar() {
  co_return 1;
}

Future<int> Foo() {
  auto r = co_await Bar();
  co_return *r + 1;
}

Future<int> Coro() {
  std::cout << "Step 1" << std::endl;
  auto r1 = co_await Foo();
  std::cout << "Step 2" << std::endl;
  auto r2 = co_await Bar();
  std::cout << "Completed" << std::endl;
  co_return *r1 + *r2;
}

// Blue (synchronous)
int NonCoro() {
  // Coroutine / non-coroutine boundary
  auto f = Coro();
  auto r = std::move(f) | futures::Get();
  return *r;
}

int main() {
  NonCoro();
  return 0;
}

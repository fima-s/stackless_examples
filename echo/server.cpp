#include "server.hpp"

#include <coroutine>

#include <asio.hpp>

#include <utility>
#include <vector>
#include <thread>

#include "../promises/std_future.hpp"

using asio::ip::tcp;

//////////////////////////////////////////////////////////////////////

// Asynchronous operations

struct ReadSomeAwaiter {
  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    socket_.async_read_some(buffer_,
                            [this, h](auto ec, auto bytes_read) mutable {
                              ec_ = ec;
                              bytes_read_ = bytes_read;
                              h.resume();
                            });
  }

  auto await_resume() {
    if (ec_) {
      throw std::system_error(ec_);
    }
    return bytes_read_;
  }

  // Arguments
  tcp::socket& socket_;
  asio::mutable_buffer buffer_;

  std::error_code ec_;
  size_t bytes_read_;
};

auto AsyncReadSome(tcp::socket& socket, asio::mutable_buffer buffer) {
  return ReadSomeAwaiter{socket, buffer};
}

struct WriteAwaiter {
  bool await_ready() {
    return false;
  }

  void await_suspend(std::coroutine_handle<> h) {
    asio::async_write(socket_, buffer_,
                      [this, h](auto ec, size_t bytes) mutable {
                        ec_ = ec;
                        h.resume();
                      });
  }

  void await_resume() {
    if (ec_) {
      throw std::system_error(ec_);
    }
  }

  // Arguments
  tcp::socket& socket_;
  asio::const_buffer buffer_;

  std::error_code ec_;
};

auto AsyncWrite(tcp::socket& socket, asio::const_buffer buffer) {
  return WriteAwaiter{socket, buffer};
}

//////////////////////////////////////////////////////////////////////

// Client Handler

std::future<void> HandleClient(tcp::socket socket) {
  static const size_t kBufferSize = 1024;

  char data[kBufferSize];

  try {
    while (true) {
      size_t bytes_read = co_await AsyncReadSome(socket, asio::buffer(data));
      co_await AsyncWrite(socket, {data, bytes_read});
    }
  } catch (...) {
    // Ok
  }
}

//////////////////////////////////////////////////////////////////////

class Server {
 public:
  Server(asio::io_context& io_context, uint16_t port)
      : acceptor_(io_context, tcp::endpoint(tcp::v4(), port)) {
  }

  void Start() {
    AcceptClient();
  }

 private:
  void AcceptClient() {
    acceptor_.async_accept(
        [this](std::error_code error_code, tcp::socket client_socket) {
          if (!error_code) {
            HandleClient(std::move(client_socket));
          }
          AcceptClient();
        });
  }

 private:
  tcp::acceptor acceptor_;
};

////////////////////////////////////////////////////////////////////////////////

void Run(asio::io_context& io_context, size_t threads) {
  std::vector<std::thread> workers;

  for (size_t i = 1; i < threads; ++i) {
    workers.emplace_back([&io_context]() {
      io_context.run();
    });
  }
  io_context.run();

  for (auto& t : workers) {
    t.join();
  }
}

void ServeForever(uint16_t port) {
  asio::io_context io_context;

  Server server(io_context, port);
  server.Start();

  Run(io_context, /*threads=*/5);
}

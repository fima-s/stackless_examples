#include <iostream>

#include <flow/coroutine.hpp>

using StackfulCoroutine = flow::SimpleCoroutine;

//////////////////////////////////////////////////////////////////////

int Foo() {
  return 7;
}

int Bar(int x) {
  return x + 1;
}

int Baz(int a, int b, int x) {
  return a * b + x;
}

// void Coro(int x) {
//  int a = Foo();
//  <suspend>
//  int b = Bar(a);
//  <suspend>
//  int c = Baz(a, b, x);
//  std::cout << c << std::endl;
//}

//////////////////////////////////////////////////////////////////////

// Hand-crafted stackless coroutine

// State:
// 1) Arguments
// 2) Local variables
// 3) Instruction pointer

class StacklessCoroutine {
 public:
  StacklessCoroutine(int x) : x_(x) {

  }

  void Resume() {
    if (label_ == 0) {
      a_ = Foo();
      label_ = 1;  // Advance instruction pointer
      return;  // Suspend
    } else if (label_ == 1) {
      b_ = Bar(a_);
      label_ = 2;
      return;
    } else if (label_ == 2) {
      int c = Baz(a_, b_, x_);
      std::cout << c << std::endl;
    }
  }

 private:
  // Arguments
  int x_;

  // Local variables
  int a_;
  int b_;

  // Instruction pointer
  int label_ = 0;
};

//////////////////////////////////////////////////////////////////////

int main() {
  int x = 5;

//  StackfulCoroutine coro([x]() {
//    int a = Foo();
//    StackfulCoroutine::Suspend();
//    int b = Bar(a);
//    StackfulCoroutine::Suspend();
//    int c = Baz(a, b, x);
//    std::cout << c << std::endl;
//  });

  StacklessCoroutine coro{x};

  std::cout << "Step 1" << std::endl;
  coro.Resume();
  std::cout << "Step 2" << std::endl;
  coro.Resume();
  std::cout << "Step 3" << std::endl;
  coro.Resume();

  return 0;
}
